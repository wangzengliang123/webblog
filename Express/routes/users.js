var express = require('express');
var router = express.Router();

const db = require('../db/db');
const sendmail = require('../sendmail/sendmail');
const md5 = require('md5');


/* GET users listing. */
router.get('/', function (req, res, next) {
  res.send('respond with a resource');
});


// 用户获取验证码
router.post('/getCode', (req, res, next) => {
  let registerInfo = req.body;

  // 判断输入是否为空
  if (registerInfo.user_name == '' || registerInfo.pwd == '' || registerInfo.email == '' || registerInfo.user_name == undefined || registerInfo.pwd == undefined || registerInfo.email == undefined) {
    let errMsg = { code: 01, msg: '注册失败，输入不能为空！' };
    res.send(errMsg);
  } else {
    // 判断邮箱是否已注册
    let sql = `select * from user where email = '${registerInfo.email}'`;
    db(sql, (err, result) => {
      if (err) throw err;
      // 不存在时,发送邮件
      if (!result.length) {
        sendmail(registerInfo);
        let successMsg = { code: 10, msg: '邮件发送成功，注意接收！' };
        res.send(successMsg);
      } else {
        let errMsg = { code: 02, msg: '注册失败，此邮箱已注册！' };
        res.send(errMsg);
      }
    })
  }
});


// 用户注册
router.post('/register',(req,res,next)=>{
  // 用户信息
  let userInfo = req.body;

  // 加密用户密码
  let md5_pwd = userInfo.pwd;

  // 验证验证码是否正确
  if(userInfo.code !== localStorage.getItem(userInfo.email)){
    let errMsg = {code:03,msg:'验证码不正确，请重新输入'};
    res.send(errMsg);
  }else{
    let sql = `insert into user values (null,'${userInfo.user_name}','${md5_pwd}','${userInfo.email}','Express/public/images/tavatar_default.png)',${userInfo.create_time}`;
    db(sql,(err,result)=>{if(err) throw err;res.send(result)});
  }
});


// 用户登录






module.exports = router;
