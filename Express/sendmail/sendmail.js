const nodemailer = require('nodemailer');

// 引入缓存模块
const LocalStorage = require('node-localstorage').LocalStorage,
localStorage = new LocalStorage('./scratch');

module.exports = function (data) {
    // 开启一个SMTP连接池
    let transporter = nodemailer.createTransport({
        host: 'stmp.qq.com',
        port: 465,
        auth: {
            user: '212547742@qq.com',
            pass: 'apbcoxsaiqdrcaje',
        },
    });

    // 创建随机六位数验证码
    let registerCode = function () {
        let num = '';
        for (let i = 0; i < 6; i++) {
            num += Math.floor(Math.random() * 10);
        };

        return num;
    };


    // 设置邮件内容
    let mailOptions = {
        from: 'ヽ(•̀ω•́ )ゝ<212547742@qq.com>', // 发件者昵称和邮箱地址
        to: data.email,     // 接收者的邮箱地址
        subject: '注册验证码',      // 邮件主题
        // html: data.content,     // 邮件html内容
        text: '您好，感谢您的注册。您的验证码为:' + registerCode(),
    };

    // 发送邮件（使用先前创建的传输器的sendMail方法传递消息对象）
    transporter.sendMail(mailOptions, (err, info) => { if (err) throw err; console.log('邮件发送成功 ID：' + info.messageId) });


    // 将验证码存储到本地缓存中，用于验证
    localStorage.setItem(data.email,registerCode());
}