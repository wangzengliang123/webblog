const mysql = require('mysql');

module.exports = function (sql,callback) {

    const connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: '',
        database: 'myblog',
        port: '3306'
    });

    connection.connect();
    connection.query(sql,callback);
    connection.end();
};