### 获取验证码(POST)
接口(./getCode)
```javascript
// 数据格式
data={
    "user_name":"111",
    "pwd":"1111",
    "email":"11111@qq.com",
} 
```

### 用户注册(POST)
接口(./register)
```javascript
// 数据格式
data={
    "user_name":"111",
    "pwd":"1111",
    "email":"11111@qq.com",
    "create_time":"2022-08-01 15:36:00" // 获取当前时间
    "code":"111111"
} 
```